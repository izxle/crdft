#!/usr/bin/env python
from argparse import ArgumentParser
from pathlib import Path
from typing import List
import re

from ase.data.pubchem import pubchem_atoms_search


def download_atoms(pubchem_names: List[str], overwrite: bool = False):
    for pubchem_name in pubchem_names:
        name = re.sub(r"[,() ]", "", pubchem_name)
        xyzfile = Path(f'{name}.xyz')
        if xyzfile.is_file() and not overwrite:
            continue
        atoms = pubchem_atoms_search(pubchem_name)
        atoms.write(xyzfile)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('filename', type=Path,
                        help='File with PubChem names to download (one on each line).')
    parser.add_argument('-o', '--overwrite', action='store_true',
                        help='Overwrite existing xyz files if they exists.')
    args = parser.parse_args()

    with args.filename.open('r') as file:
        pubchem_names = file.read().splitlines()

    download_atoms(pubchem_names, args.overwrite)
