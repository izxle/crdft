#!/usr/bin/env python
from cdft.tasks.tasks import calculate_descriptors_BOPA
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def main():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('filename', help='DeMoN outfile with MO population analysis')
    parser.add_argument('-o', default='report.txt', dest='outfile', help='name of report file')
    args = parser.parse_args()

    calculate_descriptors_BOPA(args.filename, args.outfile)


if __name__ == '__main__':
    main()
