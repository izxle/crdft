#!/usr/bin/env python
from __future__ import annotations

import re
import sys
from argparse import ArgumentParser
from pathlib import Path

from ase.io import read

from cdft.io import read_gaussian_out


def update_gaussian_input_xyz(infile: str | Path, outfile: str | Path):
    """
    Overwrite molecule specification section in Gaussian Input file.
    :param infile: str | Path
        Gaussian input file to be modified
    :param outfile: str | Path
        Gaussian output file to get the coordinates from
    :return: None
    """
    fmt = '22.15f'
    # read xyz information from output
    with open(infile, 'r') as f:
        text = f.read()

    matches = list(re.finditer(r'.+?\n\n', text, re.DOTALL))

    if not matches or len(matches) < 3:
        print(fr'Input file {infile} not in a correct gaussian format', file=sys.stderr)
        sys.exit(1)

    atoms = read(outfile, format='gaussian-out', index=-1)
    xyz = '\n'.join(f'{a.symbol:>2} {a.x:{fmt}} {a.y:{fmt}} {a.z:{fmt}}'
                    for a in atoms)

    m_link0_route = matches[0]
    m_title = matches[1]
    m_molecule = matches[2]
    charge_mult = m_molecule.group().split('\n', 1)[0]

    new_text = m_link0_route.group()
    new_text += m_title.group()
    new_text += charge_mult + '\n'
    new_text += xyz + '\n'
    new_text += text[m_molecule.end():]

    with open(infile, 'w') as f:
        f.write(new_text)


def gaussian_out2xyz(fileout: str | Path):
    """
    Write xyz file from Gaussian Output file
    :param fileout: str | Path
        Gaussian output file to get the coordinates from
    :return: None
    """
    atoms = read_gaussian_out(fileout)
    filexyz = Path(fileout).stem + '.xyz'
    atoms.write(filexyz)


def is_file(filename: str | Path):
    """
    Auxiliary function for the argument parser to check if the file exists
    """
    file = Path(filename)
    if not file.is_file():
        print(f"IOError: '{file}' is not a valid File.", file=sys.stderr)
        raise ValueError
    return file


def main():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')

    parser_input = subparsers.add_parser('update-input',
                                         help='Overwrite coordinates in Gaussian Input file from coordinates '
                                              'in Gaussian Output file')
    parser_input.add_argument('gaussian_in', type=is_file,
                              help='Gaussian Input file to be modified')
    parser_input.add_argument('gaussian_out', type=is_file,
                              help='Gaussian output file to get the coordinates from')

    parser_out2xyz = subparsers.add_parser('out2xyz',
                                           help='Write xyz file from Gaussian Output file')
    parser_out2xyz.add_argument('fileout', type=is_file,
                                help='Gaussian output file to get the coordinates from')

    args = parser.parse_args()
    if args.command == 'update-input':
        update_gaussian_input_xyz(args.gaussian_in, args.gaussian_out)
    elif args.command == 'out2xyz':
        gaussian_out2xyz(args.fileout)


if __name__ == '__main__':
    main()
