import time
from pathlib import Path
from typing import Tuple, List, Dict

import numpy as np
from ase import Atoms
from ase.io import read


def read_dSCF_fukui_gpaw(name: str, suffix: str = '.gpw') -> Tuple[List[str], float, float, np.ndarray, np.ndarray]:
    from gpaw.analyse.hirshfeld import HirshfeldPartitioning
    from gpaw import restart
    print(name)
    data: Dict[int, Atoms] = dict()
    partial_charges: Dict[int, np.ndarray] = dict()
    for charge in {0, -1, 1}:
        # read calculation results
        file_calc = f'{name}_{charge}{suffix}'
        file_charges = Path(f'{name}_{charge}_hirshfeld.xyz')

        if file_charges.is_file():
            # TODO: add calc info to Hirshfeld.traj
            atoms = read(file_charges)
            partial_charges[charge] = np.array(atoms.get_initial_charges())
        else:
            print('Reading', file_calc)
            st = time.process_time()
            restart(file_calc, txt=None)
            print('   ... ', time.process_time() - st, 'seconds')

            print('calculating hirshfeld charges from', file_calc)
            # read calculation results
            file_calc = f'{name}_{charge}{suffix}'
            atoms, calc = restart(file_calc, txt=None)

            hf = HirshfeldPartitioning(calc)
            charges_i = np.array(hf.get_charges())
            atoms.set_initial_charges(charges_i)
            partial_charges[charge] = charges_i
            atoms.write(file_charges)
        data[charge] = atoms
    st = time.process_time()

    labels = atoms.get_chemical_symbols()

    I = data[1].get_potential_energy() - data[0].get_potential_energy()
    A = data[0].get_potential_energy() - data[-1].get_potential_energy()

    # TODO: check if nucleophile/electrophile affects calculation
    fukui_plus = partial_charges[0] - partial_charges[-1]
    fukui_minus = partial_charges[1] - partial_charges[0]

    print('   got energies:', time.process_time() - st, 'seconds')

    if sum(fukui_minus) - 1 > 1e-2 or sum(fukui_plus) - 1 > 1e-2:
        breakpoint()

    return labels, I, A, fukui_plus, fukui_minus
