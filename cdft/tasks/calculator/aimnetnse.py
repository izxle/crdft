import time
from pathlib import Path
from typing import Dict

import numpy as np
from ase import Atoms
from ase.io import read


def read_dSCF_fukui_aimnetnse(name, suffix):
    data: Dict[int, Atoms] = dict()
    partial_charges: Dict[int, np.ndarray] = dict()
    for charge in {0, -1, 1}:
        # read calculation results
        file_charges = Path(f'{name}_{charge}_aimnetnse.xyz')

        if file_charges.is_file():
            atoms = read(file_charges)
            partial_charges[charge] = np.array(atoms.get_initial_charges())
        else:
            raise IOError(f'AIMNetNSE atoms file not found. {file_charges}')
        data[charge] = atoms
    st = time.process_time()

    labels = atoms.get_chemical_symbols()

    I = data[1].get_potential_energy() - data[0].get_potential_energy()
    A = data[0].get_potential_energy() - data[-1].get_potential_energy()

    # TODO: check if nucleophile/electrophile affects calculation
    fukui_plus = partial_charges[0] - partial_charges[-1]
    fukui_minus = partial_charges[1] - partial_charges[0]

    print('   got energies:', time.process_time() - st, 'seconds')

    if sum(fukui_minus) - 1 > 1e-2 or sum(fukui_plus) - 1 > 1e-2:
        breakpoint()

    return labels, I, A, fukui_plus, fukui_minus
