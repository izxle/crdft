
script = '''#!/bin/bash
#SBATCH --partition=$QUEUE
#SBATCH --job-name=$JOBNAME
#SBATCH --ntasks=$NPROC
#SBATCH --time=$TIME
#SBATCH --error=$ERR
#SBATCH --output=$OUT
$EXTRA_OPTIONS

echo "$SLURM_JOB_NAME $SLURM_JOB_ID user: $USER"
echo "Time at submission: $(date)"

cd "$SLURM_SUBMIT_DIR"

# load necessary modules
module purge
$MODULES

srun hostname -s | sort > ${SLURM_JOB_NAME}_hostlist.dat

init=$(date +%s)
$COMMAND
fin=$(date +%s)

echo "Total execution time: $(( fin - init )) s"
'''

# name, label, default
values = (
    ('jobname', '$JOBNAME', 'test'),
    ('nproc', '$NPROC', '1'),
    ('time', '$TIME', '01-00:00:00'),
    ('error', '$ERR', '%x_%j.err'),
    ('out', '$OUT', '%x_%j.out'),
    ('modules', '$MODULES', '# no additional modules loaded'),
    ('command', '$COMMAND', 'echo test'),
)


def build_SLURM_script(data: dict, **kwargs):
    data = data.copy()
    data.update(**kwargs)
    res = script.replace('$QUEUE', data['queue'])
    for name, label, default in values:
        res = res.replace(label, data.get(name, default))
    names = [v[0] for v in values]
    extra_keys = [k for k in data if k not in names + ['queue']]
    extra_options = ''
    for name in extra_keys:
        extra_options += f'#SBATCH --{name}={data[name]}\n'
    res = res.replace('$EXTRA_OPTIONS', extra_options)
    return res
