import re
from itertools import combinations, product
from pathlib import Path
from typing import List, Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ase.io import read
from ase.units import Hartree
from ase.utils import workdir
from matplotlib.ticker import MaxNLocator
from scipy.optimize import minimize
from sklearn.linear_model import LinearRegression
from tqdm import tqdm

from cdft.descriptors import OneParabola, DescriptorsModel, ChargeTransferPP, ChargeTransfer2P, ChargeTransferFamily, \
    model_suffix, ChargeTransferGQ
from cdft.io import read_from_CT_directory, read_gaussian_out
from cdft.tasks.calculator.aimnetnse import read_dSCF_fukui_aimnetnse
from cdft.tasks.calculator.gaussian import read_atoms_charges_gaussian, read_dSCF_fukui_gaussian
from cdft.tasks.calculator.gpaw import read_dSCF_fukui_gpaw
from cdft.tasks.config import RunConfiguration


def read_BOPA_DeMoN(outfile):
    """
    read from Demon outfile
    """
    # read outfile
    with open(outfile, 'r') as f:
        text = f.read()

    # capture HOMO and LUMO orbital numbers and text blocks for fukui functions
    # TODO: Add BETA MOs?
    regex = re.compile(r'\s MO ENERGIES OF CYCLE \d+\s+(?P<MOE>.+?)\s+RANDOMIZED SCF.+'
                       r'ALPHA MO: +(?P<HOMO>\d+)\s+(?P<fminus>.+?)\s+ALL.+'
                       r'ALPHA MO: +(?P<LUMO>\d+)\s+(?P<fplus>.+?)\s+ALL', re.DOTALL)
    m = regex.search(text)
    if m is None:
        raise IOError(f'MO analysis not found in outfile')

    # get fukui functions from MO population analysis
    fukui_minus_str = m.group('fminus')
    fukui_minus = [float(line.split()[-1])
                   for line in fukui_minus_str.split('\n')]
    fukui_plus_str = m.group('fplus')
    fukui_plus = [float(line.split()[-1])
                  for line in fukui_plus_str.split('\n')]

    atoms_lbl = [line.split()[1] + line.split()[0]
                 for line in fukui_minus_str.split('\n')]

    # extract MO energies from text block
    text_MOE = m.group('MOE').replace('**********', 'NaN')
    lines_MOE = text_MOE.split('\n')
    n_blocks = len(lines_MOE) // 8
    energies = list()
    for i_block in range(n_blocks):
        i = i_block * 8
        str_MO, str_E, *_ = lines_MOE[i: i + 8]
        Es_i = [float(E_i) for E_i in str_E.split()]
        energies.extend(Es_i)

    # get HOMO and LUMO indices
    i_HOMO = int(m.group('HOMO')) - 1
    i_LUMO = int(m.group('LUMO')) - 1

    # get IE and EA from HOMO and LUMO in eV
    IE = -energies[i_HOMO] * Hartree
    EA = -energies[i_LUMO] * Hartree

    return atoms_lbl, IE, EA, fukui_plus, fukui_minus


def read_BOPA(filename, outformat):
    if outformat == 'DeMoN':
        res = read_BOPA_DeMoN(filename)
    else:
        raise NotImplementedError(f'Format `{outformat}` not implemented')
    return res


def calculate_descriptors_BOPA(name: str, outfile: str = 'report.txt', outformat: str = 'DeMoN'):
    ns = 7
    ff = f'{ns}.4f'

    atoms_lbl, I, A, fukui_plus, fukui_minus = read_BOPA(name, outformat)
    # TODO: update building of descriptors object
    ds = OneParabola(name, I, A, fukui_plus, fukui_minus)

    report = 'Global Descriptors\n'
    report += f'\tI = {ds.I:{ff}}eV\tA = {ds.A:{ff}}eV\tµ = {ds.mu:{ff}}\tη = {ds.eta:{ff}}\n'
    # header for local descriptors
    # TODO: check references to descriptors
    s2 = " " * (ns - 2)
    s3 = " " * (ns - 3)
    report += f'Local Descriptors\n  atom {s2}f- {s2}f+ {s2}f0 {s3}df {s2}µ- {s2}µ+ {s2}µ0 ' \
              f"{s2}η- {s2}η+ {s2}η0 {s3}η'- {s3}η'+ {s3}η'0 {s2}s- {s2}s+ {s2}s0\n"
    # local descriptors for each atom
    report += '\n'.join(f'{a:>6} '
                        f'{ds.f_minus(i):{ff}} '
                        f'{ds.f_plus(i):{ff}} '
                        f'{ds.f_zero(i):{ff}} '
                        f'{ds.df(i):{ff}} '
                        f'{ds.mu_minus(i):{ff}} '
                        f'{ds.mu_plus(i):{ff}} '
                        f'{ds.mu_zero(i):{ff}} '
                        f'{ds.eta_minus(i):{ff}} '
                        f'{ds.eta_plus(i):{ff}} '
                        f'{ds.eta_zero(i):{ff}} '
                        f'{ds.etact_minus(i):{ff}} '
                        f'{ds.etact_plus(i):{ff}} '
                        f'{ds.etact_zero(i):{ff}} '
                        f'{ds.s_minus(i):{ff}} '
                        f'{ds.s_plus(i):{ff}} '
                        f'{ds.s_zero(i):{ff}}'
                        for i, a in enumerate(atoms_lbl))
    # non-local descriptors
    report += f'\nNon-local Descriptors\n    atoms {s2}f- {s2}f+ {s2}f0 {s3}df {s2}µ- {s2}µ+ {s2}µ0 ' \
              f"{s2}η- {s2}η+ {s2}η0 {s3}η'- {s3}η'+ {s3}η'0 {s2}s- {s2}s+ {s2}s0\n"
    # TODO: filter relevant atoms
    combs = list()
    rex = re.compile(r'\d+')
    for ai, aj in combinations(atoms_lbl, 2):
        if 'H' in ai or 'H' in aj:
            continue
        i = int(rex.search(ai).group()) - 1
        j = int(rex.search(aj).group()) - 1
        lbl = f'{ai}-{aj}'
        combs.append((i, j, lbl))
    report += '\n'.join(f'{lbl:>9} '
                        f'{ds.f_minus_nonlocal(i, j):{ff}} '
                        f'{ds.f_plus_nonlocal(i, j):{ff}} '
                        f'{ds.f_zero_nonlocal(i, j):{ff}} '
                        f'{ds.df_nonlocal(i, j):{ff}} '
                        f'{ds.mu_minus(i, j):{ff}} '
                        f'{ds.mu_plus(i, j):{ff}} '
                        f'{ds.mu_zero(i, j):{ff}} '
                        f'{ds.eta_minus(i, j):{ff}} '
                        f'{ds.eta_plus(i, j):{ff}} '
                        f'{ds.eta_zero(i, j):{ff}} '
                        f'{ds.etact_minus(i, j):{ff}} '
                        f'{ds.etact_plus(i, j):{ff}} '
                        f'{ds.etact_zero(i, j):{ff}} '
                        f'{ds.s_minus(i, j):{ff}} '
                        f'{ds.s_plus(i, j):{ff}} '
                        f'{ds.s_zero(i, j):{ff}}'
                        for i, j, lbl in combs)

    with open(outfile, 'w') as f:
        f.write(report + '\n')


def read_dSCF_fukui(name: str, suffix: str, calc_name: str):
    if calc_name == 'gpaw':
        labels, I, A, fukui_plus, fukui_minus = read_dSCF_fukui_gpaw(name, suffix)
    elif calc_name == 'gaussian':
        labels, I, A, fukui_plus, fukui_minus = read_dSCF_fukui_gaussian(name, suffix)
    elif calc_name == 'aimnetnse':
        labels, I, A, fukui_plus, fukui_minus = read_dSCF_fukui_aimnetnse(name, suffix)
    else:
        raise ValueError(f'Format for `{calc_name}` not yet implemented.')
    return labels, I, A, fukui_plus, fukui_minus


def calculate_descriptors_CT(config: RunConfiguration):
    names_A = list(config.analysis.data.keys())
    name_B = config.analysis.reagent
    directory = Path('calculations')

    # check that all calculations have finished
    for name in tqdm(names_A, desc='Checking for unfinished calculations'):
        with workdir(directory / name), open(f'{name}.xyz'):
            pass

    # get descriptors for list of molecules
    ds_A: Dict[str, DescriptorsModel] = dict()
    for name in tqdm(names_A, desc='Reading Hirshfeld charges'):
        with workdir(directory / name):
            atoms_lbl, I, A, fukui_plus, fukui_minus = read_dSCF_fukui(name, config.calc_out_suffix, config.calc_name)
            ds = OneParabola(name, I, A, fukui_plus, fukui_minus)
            ds_A[name] = ds

    # get descriptors for reagent
    with workdir(directory / name_B):
        atoms_lbl, I, A, fukui_plus, fukui_minus = read_dSCF_fukui(name_B, config.calc_out_suffix, config.calc_name)
        ds_B = OneParabola(name_B, I, A, fukui_plus, fukui_minus)

    cts: List[ChargeTransferPP] = list()
    exp_constants: List[float] = list()
    for (name, A), B in tqdm(product(ds_A.items(), [ds_B]), total=len(ds_A), desc='Building ChargeTransfer objects'):
        # build ChargeTransfer object
        ct = ChargeTransferPP(A, B)
        cts.append(ct)
        exp_constants.append(config.analysis.data[name])
        with workdir(directory / name):
            with open('charge_transfer.txt', 'w') as f:
                print(ct, file=f)

    print('Builiding final report...')
    ctF = ChargeTransferFamily(cts, exp_constants)
    # plot correlation with experimental data
    if config.analysis.group:
        ctF.plot_group(config.analysis.type, config.analysis.local_selection, config.analysis.group)
        a, b = config.analysis.group
        report = ctF.report_local(a, b, config.analysis.type, config.analysis.local_selection)
        with open(f'charge_transfer_{a}-{b}.txt', 'w') as f:
            print(report, file=f)
    else:
        ctF.report(config.analysis.type)
        ctF.plot(config.analysis.type)
    print('Done.')


def charge_transfer_analysis(config: RunConfiguration):
    names_A = list(config.analysis.data.keys())
    name_B = config.analysis.reagent
    directory = Path(config.directory_out)
    print('Starting charge transfer analysis...')
    for model_name in config.analysis.model:
        print(f'Analyzing with the model: {model_name}')
        model = model_suffix[model_name]
        # Build DescriptorsModel for each ChargeTransfer calculation directory reading the previously generated extxyz files
        ds_A: Dict[str, DescriptorsModel] = {name: read_from_CT_directory(directory / name, model=model)
                                             for name in names_A}
        # Build DescriptorsModel for molecule B
        ds_B = read_from_CT_directory(directory / name_B, model=model)

        if model == 'PP':
            ChargeTransfer = ChargeTransferPP
        elif model == '2P':
            ChargeTransfer = ChargeTransfer2P
        elif model == 'GQ':
            ChargeTransfer = ChargeTransferGQ
        else:
            raise ValueError('Check ME.')

        # Create the ChargeTransfer objects
        cts = [ChargeTransfer(A, ds_B) for name, A in ds_A.items()]

        # Get experimental constants and Mayr data
        exp_constants = list()
        data_mayr = dict(N=list(), SN=list(), E=list())
        for name in ds_A:
            exp_constants.append(config.analysis.data[name])
            if name in config.analysis.data_mayrN:
                data_mayr['N'].append(config.analysis.data_mayrN[name])
            if name in config.analysis.data_mayrSN:
                data_mayr['SN'].append(config.analysis.data_mayrSN[name])
            if name in config.analysis.data_mayrE:
                data_mayr['E'].append(config.analysis.data_mayrE[name])

        # Crate final report
        ctF = ChargeTransferFamily(cts, exp_constants, data_mayr)
        ct_type = config.analysis.type
        local_group = config.analysis.group
        local_selection_type = config.analysis.local_selection

        # Build local report
        if local_group:
            # Report a specific local group
            a, b = local_group
            report_str = ctF.report_local(a, b, ct_type, local_selection_type)
            with open(f'charge_transfer{model}_{a}-{b}.txt', 'w') as f:
                print(report_str, file=f)
            ctF.plot_group(ct_type, local_selection_type, local_group)
        else:
            # Report global model and the best local correlation
            ctF.report(ct_type, local_selection_type, label=model)
            ctF.plot(ct_type, local_selection_type, label=model)
            (a, b), _ = ctF.get_best_group(ct_type, local_selection_type)

        # Write a report for each molecule
        for name, ct, logk in zip(names_A, cts, exp_constants):
            report_str = ct.report(ct_type, a, b, local_selection_type, logk=logk)
            with workdir(directory / name):
                with open(f'charge_transfer{model}_{a}-{b}.txt', 'w') as f:
                    print(report_str, file=f)

    # find outliers
    x = np.abs(ctF.dNA_global(ct_type))
    y = ctF.exp_values
    x_i = list(x)
    y_i = list(y)
    rs = list()
    names = list(names_A)
    removed = ['']
    for _ in range(len(x), 1, -1):
        r_sq, intercept, slope = correlate(x_i, y_i)
        rs.append(r_sq)
        errors = abs(np.array(x_i) * slope + intercept - y_i)
        i_maxerr = np.argmax(errors)
        removed.append(names[i_maxerr])
        del x_i[i_maxerr], y_i[i_maxerr], names[i_maxerr]
    plt.figure()
    plt.plot(list(range(len(x), 1, -1)), rs, '-o')
    plt.axhline(y=0.9, linestyle='--', color='black')
    plt.xlabel('$N_k$')
    plt.ylabel('$r^2$')
    plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.savefig('corr_outlier.png')
    text = 'logk ΔNA\n'
    text += '\n'.join(f'{xi} {yi}'
                      for xi, yi in zip(x, y))
    text += '\n\nk r2 outlier\n'
    text += '\n'.join(f'{i} {r2} {rx}'
                      for i, r2, rx in zip(range(len(x), 1, -1), rs, removed))
    with open('corr_outlier.dat', 'w') as f:
        f.write(text + '\n')

    print('Done.')


def electrophilicity_analysis(config: RunConfiguration):
    directory = config.directory_out
    model = config.analysis.model
    df = pd.read_csv(config.analysis.data_mayr, index_col=0)
    molecules = df['Molecule'].str.replace('[(), !\[\]]', '')
    data: Dict[str, Dict[str, List]] = dict()
    for filename in config.filenames:
        name = filename.stem
        if name in config.analysis.ignore:
            continue
        outdir = directory / name
        try:
            ct = read_from_CT_directory(outdir, model=model)
        except FileNotFoundError:
            print(f'skipping {name}: Calculation not yet finished.')
            continue
        il = molecules == name
        if not any(il):
            print(f'Skipping {name}: reference value not found in database.')
            continue

        row = df[il]
        # TODO: Change to include both structures
        row = df[il].iloc[0, :]

        group = row.Class.split('/')[-1].split(' (')[0]
        # skip if group in exclusion list or if not in groups list
        if group in config.analysis.exclude or config.analysis.groups and group not in config.analysis.groups:
            continue

        reactivity_parameters = row['Reactivity Parameters']

        if 'N Parameter' in reactivity_parameters:
            w = ct.omega_minus
            ref = row['N']
            labels = ('N', r'\omega^-')
        elif 'E Parameter' in reactivity_parameters:
            w = ct.omega_plus
            ref = row['E']
            labels = ('E', r'\omega^+')
        else:
            raise ValueError(f'Unexpected value for reactivity parameters: {reactivity_parameters}')

        if group in data:
            data[group]['name'].append(name)
            data[group]['w'].append(w)
            data[group]['ref'].append(ref)
        else:
            data[group] = dict(name=[name], w=[w], ref=[ref], labels=labels)

    text = ''
    for group, data_group in data.items():
        if len(data_group['name']) < 3:
            print(f'Skipping {group}: not enough values to correlate.')
            continue

        plt.figure()
        NE, wlbl = data_group.pop('labels')

        max_len = max(len(name) for name in data_group['name'])

        x = data_group['w']
        y = data_group['ref']
        plt.scatter(x, y, label=group)
        r_sq, intercept, slope = correlate(x, y)
        y_ = slope * np.array(x) + intercept

        text += f'{group}\tr^2 = {r_sq:.4f}\n\t{"system":{max_len}} {"ω" + wlbl[-1]:^8}   {NE}\n'
        text += '\n'.join(f'\t{name:{max_len}} {w:8.3f} {ref:5.1f}'
                          for name, w, ref in zip(*data_group.values()))
        text += '\n'

        plt.plot(x, y_, label=f'$\mathrm{{{NE}}}^{{Mayr}}={slope:.2f}{wlbl}{intercept:+.2f}; r^2 = {r_sq:.2f}$')
        plt.xlabel(rf'${wlbl}$')
        plt.ylabel(f"$\mathrm{{{NE}}}^{{Mayr}}$")
        plt.legend(loc='best')
        plt.savefig(f'electrophilicity_analysis_r{r_sq:.2f}_{group}.png', dpi=300)
        plt.close()

    # plt.xlabel(r'$\omega$')
    # plt.ylabel("Mayr's reactivity parameters")
    # # plt.ylabel(f"$\mathrm{{{NE}}}^{{Mayr}}$")
    # plt.legend(loc='best')
    # plt.savefig('electrophilicity_analysis.png', dpi=300)
    # plt.show()

    with open(f'data_{model}.txt', 'w') as f:
        print(text, file=f)

    plt.show()
    print('Done.')


def electrophilicity_gamma_optimization(config: RunConfiguration):
    directory = config.directory_out
    model = config.analysis.model

    df = pd.read_csv(config.analysis.data_mayr, index_col=0)
    molecules = df['Molecule'].str.replace('[(), !\[\]]', '')

    def func_single(g, NE, I, A):
        return NE - ((g * I + A) ** 2) / ((1 + g) ** 2 * (I - A))

    def func_group(g, NE, I, A):
        x = ((g * I + A) ** 2) / ((1 + g) ** 2 * (I - A))
        y = NE
        r_sq, intercept, slope = correlate(x, y)
        y_pred = x * slope + intercept
        err = sum(abs(y_pred - y) ** 2)
        return err

    if config.analysis.groups:
        groups = config.analysis.groups
    else:
        groups = [filename.relative_to(config.directory_in).parent
                  for filename in config.filenames]

    groups = ["Electrophiles (352)/C-Electrophiles (327)/Carbocations (132)/Diarylcarbenium Ions (33)"]
    data: Dict[Path, Dict[str, List]] = {g: dict(name=[], w=[], I=[], A=[], ref=[])
                                         for g in groups}

    for group in tqdm(groups):
        for filename in config.filenames:
            name = filename.stem
            if name in config.analysis.ignore:
                continue

            outdir = directory / name
            try:
                ct = read_from_CT_directory(outdir, model=model)
            except FileNotFoundError:
                print(f'skipping {name}: Calculation not yet finished.')
                continue
            il = molecules == name
            if not any(il):
                print(f'Skipping {name}: reference value not found in database.')
                continue

            row = df[il]
            # TODO: Change to include both structures
            row = df[il].iloc[0, :]

            group = row.Class.split('/')[-1].split(' (')[0]
            # skip if group in exclusion list or if not in groups list
            if group in config.analysis.exclude or config.analysis.groups and group not in groups:
                continue

            reactivity_parameters = row['Reactivity Parameters']

            if 'N Parameter' in reactivity_parameters:
                w = ct.omega_minus
                ref = row['N']
            elif 'E Parameter' in reactivity_parameters:
                w = ct.omega_plus
                ref = row['E']
            else:
                raise ValueError(f'Unexpected value for reactivity parameters: {reactivity_parameters}')
            I = ct.I
            A = ct.A
            if group in data:
                data[group]['name'].append(name)
                data[group]['w'].append(w)
                data[group]['I'].append(I)
                data[group]['A'].append(A)
                data[group]['ref'].append(ref)

    breakpoint()
    for group, data_group in data.items():
        if len(data_group['name']) < 3:
            print(f'Skipping {group}: not enough values to correlate.')
            continue

        guess = 0.5
        ref = np.array(data_group['ref'])
        I = np.array(data_group['I'])
        A = np.array(data_group['A'])
        result = minimize(func_group, guess, args=(ref, I, A))
        breakpoint()


def perturbed_parameter_optimization(config: RunConfiguration):
    names_A = list(config.analysis.data.keys())
    name_B = config.analysis.reagent
    directory = Path(config.directory_out)
    model = config.analysis.model[0]

    # Build DescriptorsModel for each ChargeTransfer calculation directory reading the previously generated extxyz files
    ds_A: Dict[str, DescriptorsModel] = {name: read_from_CT_directory(directory / name, model=model)
                                         for name in names_A}
    # Build DescriptorsModel for molecule B
    ds_B = read_from_CT_directory(directory / name_B, model=model)

    def dNMiranda(IA, AA, IB, AB, gammas, zetas):
        return - (gammas * (IA - AB) - IB + AA) / (zetas * (1 + gammas) * (IA + IB - AA - AB))

    def dNVela(IA, AA, IB, AB, gammas, zetas):
        return (1 + gammas) * (IB - AA * gammas * (AB - AA)) / ((zetas * gammas) * (IA + IB - AA - AB))

    def dNVela4D(IA, AA, IB, AB, gammasA, gammasB, zetasA, zetasB):
        return ((1 + gammasB) * (IA - AA * gammasA) - (1 + gammasA) * (IB * gammasB - AB) /
                (1 + gammasA) * (1 + gammasB) * (zetasA * (IA - AA) + zetasB * (IB - AB)))

    def dNOX(IA, AA, IB, AB, gammas, zetas):
        return (gammas * (AA - IB) + IA - AB) / (zetas * (1 + gammas) * (IA + IB - AA - AB))

    def donating(I, A, gammas, zetas):
        return (gammas * I + A) ** 2 / (2 * (1 + gammas) ** 2 * zetas * (I - A))

    start = 0.02
    stop = 5
    n_vals = 250
    # experimental reaction constants
    # y = np.array([v for v in config.analysis.data.values()])

    # mayrs database values
    df = pd.read_csv('/Users/izxle/Documents/python-packages/cdft/data/MayrDatabase.csv', index_col=0)
    molecules = df.Molecule.str.replace('[(), !\[\]]', '')
    # names = {"5-aminoindole": "5-amino-indole",
    #          "5-chloroindole": "5-chloro-indole",
    #          "5-cyanoindole": "5-cyano-indole",
    #          "5-hydroxyindole": "5-hydroxy-indole",
    #          "5-methoxyindole": "5-methoxy-indole",
    #          "5-methylindole": "5-methyl-indole",
    #          "indole": "Indole",
    #          "indole-5-carboxylic": "1H-indole-5-carboxylicacid",
    #          }
    # y = [df.N[molecules == names[name]].values[0] for name in names_A]

    y = [df.N[molecules == name].values[0] for name in names_A]

    X = np.tile(np.linspace(start, stop, n_vals), (n_vals, 1))
    Y = X.copy().T
    # dNs = [dNOX(ds.I, ds.A, ds_B.I, ds_B.A, X, Y) for ds in ds_A.values()]
    # dNs = [dNVela4D(ds.I, ds.A, ds_B.I, ds_B.A, GA, GB, ZA, ZB) for ds in ds_A.values()]
    dNs = [donating(ds.I, ds.A, X, Y) for ds in ds_A.values()]

    Z = np.zeros(X.shape)
    Z1 = 0
    for i, j in tqdm(product(range(n_vals), range(n_vals)), desc='performing regressions', total=n_vals ** 2):
        x = [matrix[i, j] for matrix in dNs]
        r_sq, intercept, slope = correlate(x, y)
        Z[i, j] = r_sq
        if X[i, j] == 1 and Y[i, j] == 1:
            Z1 = r_sq

    # a = np.linspace(start, stop, n_vals)
    # rs = list()
    # dns = list()
    # for ga, gb, za, zb in tqdm(product(a, a, a, a), desc='performing regressions', total=n_vals ** 4):
    #     x = [dNVela4D(ds.I, ds.A, ds_B.I, ds_B.A, ga, gb, za, zb) for ds in ds_A.values()]
    #     r_sq, intercept, slope = correlate(x, y)
    #     dns.append(x)
    #     rs.append(r_sq)
    #
    # breakpoint()

    i, j = np.unravel_index(Z.argmax(), Z.shape)
    with open('perturbed_r2.txt', 'w') as f:
        print(f"Best gamma={X[i, j]:.2f} zeta={Y[i, j]:.2f} with r2={Z[i, j]:.5f}", file=f)
        print(f"gamma=1 zeta=1 r2={Z1:.5f}", file=f)
        print(f"diffrence = {Z[i, j] - Z1}", file=f)

    ax = plt.subplot(projection='3d')
    ax.plot_surface(X, Y, Z, edgecolor='royalblue', lw=0.5, rstride=8, cstride=8,
                    alpha=0.3)
    ax.contourf(X, Y, Z, zdir='z', offset=-100, cmap='coolwarm')
    ax.contourf(X, Y, Z, zdir='x', offset=-40, cmap='coolwarm')
    ax.contourf(X, Y, Z, zdir='y', offset=40, cmap='coolwarm')
    ax.set(xlabel='$\gamma$', ylabel='$\zeta$', zlabel='$r^2$')
    plt.savefig('3dplot.png', dpi=300)
    plt.show()
    # breakpoint()


def plot_perturbed_gammas(config: RunConfiguration):
    directory = Path(config.directory_out)
    model = config.analysis.model

    df = pd.read_csv(config.analysis.data_mayr, index_col=0)
    molecules = df.Molecule.str.replace('[(), !\[\]]', '')

    def wplus(I, A, gammas):
        return (gammas * I + A) ** 2 / (2 * (I - A) * (1 + gammas) ** 2)

    def wminus(I, A, gammas):
        return (gammas * A + I) ** 2 / (2 * (I - A) * (1 + gammas) ** 2)

    gammas = np.linspace(0.1, 5, 500)

    Ns = list()
    Es = list()
    yNs = list()
    yEs = list()

    all_solvents = df.Solvent.unique().tolist()
    all_solvents.remove(np.nan)
    all_solvents.remove('0')
    all_solvents.remove('0.0')
    all_solvents.append('N/A')

    data = dict(N={k: list() for k in all_solvents + ['all_w']},
                E={k: list() for k in all_solvents + ['all_w']})
    for filename in config.filenames:
        name = filename.stem
        outdir = directory / name
        try:
            ct = read_from_CT_directory(outdir, model=model)
        except FileNotFoundError:
            print(f'skipping {name}: Calculation not yet finished.')
            continue

        il = molecules == name
        if not any(il):
            print(f'Skipping {name}: reference value not found in database.')
            continue
        row = df[il].iloc[0, :]
        reactivity_parameters = row['Reactivity Parameters']
        solvent = row.Solvent

        if solvent in [np.nan, '0', '0.0']:
            solvent = 'N/A'

        if 'N Parameter' in reactivity_parameters:
            if ct.I < 0:
                print(f'skipping {name}: I < 0, I={ct.I}')
                continue

            w = wminus(ct.I, ct.A, gammas)
            ref = row['N']
            labels = ('N', r'\omega^-')
            Ns.append(w)
            yNs.append(ref)

            data['N']['all_w'].append((w, ref))
            data['N'][solvent].append((w, ref))

        elif 'E Parameter' in reactivity_parameters:
            w = wplus(ct.I, ct.A, gammas)
            ref = row['E']
            labels = ('E', r'\omega^+')
            Es.append(w)
            yEs.append(ref)

            data['E']['all_w'].append((w, ref))
            data['E'][solvent].append((w, ref))

        else:
            raise ValueError(f'Unexpected value for reactivity parameters: {reactivity_parameters}')

    rE = list()
    for i in range(len(gammas)):
        xE = [vals[i] for vals in Es]
        r_sq, intercept, slope = correlate(xE, yEs)
        rE.append(r_sq)

    for NE, dataw in data.items():
        for solvent, xy in dataw.items():
            nvals = len(xy)
            if nvals < 3:
                continue
            ws, y = zip(*xy)
            rs = list()
            for i in range(len(gammas)):
                w = [vals[i] for vals in ws]
                r_sq, intercept, slope = correlate(w, y)
                rs.append(r_sq)

            if not rs:
                breakpoint()
                continue

            plt.figure()
            plt.plot(gammas, rs)
            plt.xlabel('$\gamma$')
            plt.ylabel('$r^2$')
            plt.ylim([0, 1])
            plt.title(f'{NE} - {solvent} ({nvals})')
            plt.savefig(f'{NE}_{solvent.replace("/", "")}.png', dpi=300)

    # plt.figure()
    # plt.scatter(xE, yEs)
    # plt.title('E')

    # rN = list()
    # for i in range(len(gammas)):
    #     xN = [vals[i] for vals in Ns]
    #     r_sq, intercept, slope = correlate(xN, yNs)
    #     rN.append(r_sq)
    #
    # plt.figure()
    # plt.scatter(xN, yNs)
    # plt.title('E')

    # plt.figure()
    # plt.plot(gammas, rE, label='E')
    # plt.plot(gammas, rN, label='N')
    # plt.legend()
    # plt.xlabel('$\gamma$')
    # plt.ylabel('$r^2$')
    # breakpoint()
    # plt.show()


def test_gamma_mayr(config: RunConfiguration):
    directory = Path(config.directory_out)
    model = config.analysis.model

    df = pd.read_csv(config.analysis.data_mayr, index_col=0)
    molecules = df.Molecule.str.replace('[(), !\[\]]', '')

    all_solvents = df.Solvent.unique().tolist()
    all_solvents.remove(np.nan)
    all_solvents.remove('0')
    all_solvents.remove('0.0')
    all_solvents.append('N/A')

    data = dict(g=list(), z=list(),
                solvent={s: list() for s in all_solvents})
    for filename in config.filenames:
        name = filename.stem
        outdir = directory / name
        try:
            ds = read_from_CT_directory(outdir, model=model)
        except FileNotFoundError:
            print(f'skipping {name}: Calculation not yet finished.')
            continue

        I = ds.I
        A = ds.A
        if name == 'DABCOinMeCN':
            breakpoint()
        il = molecules == name
        if not any(il):
            print(f'Skipping {name}: reference value not found in database.')
            continue
        row = df[il].iloc[0, :]
        solvent = row.Solvent

        if row.Class != 'Nucleophiles (1264)/N-Nucleophiles (311)/Aliphatic amines (104)' or solvent != 'MeCN':
            continue

        reactivity_parameters = row['Reactivity Parameters']

        if solvent in [np.nan, '0', '0.0']:
            solvent = 'N/A'

        if 'E Parameter' in reactivity_parameters:
            continue

        if I < 0:
            print(f'skipping {name}: I < 0, I={I}')
            continue

        N = row.N
        n = I - A
        nn = N * n
        if nn < 0:
            print(f'skipping {name}: nn < 0. I={I:.2f} A={A:.2f} N={N:.2f}')
            continue

        d = n * nn ** 0.5
        gamma_plus = (nn - I * A + d) / (I ** 2 - nn)
        gamma_minus = (nn - I * A - d) / (I ** 2 - nn)

        gamma = max(gamma_plus, gamma_minus)

        def f(z, w, N):
            return abs(w / z - N)

        w = (gamma * I + A) ** 2 / (2 * (1 + gamma) ** 2 * (I - A))

        result = minimize(f, 1, args=(w, N), method='Nelder-Mead')
        zeta = result.x

        data['g'].append(gamma)
        data['z'].append(zeta)
        data['solvent'][solvent].append(gamma)

    for solvent in all_solvents + ['N/A']:
        vals = data['solvent'][solvent]
        if len(vals) < 4:
            continue
        plt.figure()
        plt.hist(vals, 50)
        plt.xlabel('$\gamma$')
        plt.title(f'Nucleophiles - {solvent} ({len(vals)})')
        plt.savefig(f'amines_gamma_{solvent.replace("/", "")}.png', dpi=300)

    #breakpoint()


def flp_analysis(config: RunConfiguration):
    """Frustrated Lewis Pairs"""
    print('Doing FLP Analysis...')
    filenames = sorted(config.filenames.copy())
    ref_name = config.analysis.reference
    i_reference = [f.name for f in filenames].index(ref_name)
    file_reference = filenames.pop(i_reference)
    reference_ref = read(file_reference).get_potential_energy()

    outdir = config.directory_out
    subdir = file_reference.relative_to(config.directory_in).parent
    file_calc_reference = outdir / subdir / file_reference.stem / file_reference.name
    reference = read(file_calc_reference).get_potential_energy()

    names = list()
    subdirs = list()
    E_reagent = list()
    E_product = list()
    dEs = list()
    E_reagent_ref = list()
    E_product_ref = list()
    dEs_ref = list()
    errors = list()
    w_global = list()
    w_local = list()
    w_force = list()

    filename_pairs = zip(filenames[::2], filenames[1::2])
    for i_model, model in enumerate(config.analysis.models):
        for file_reagent, file_product in filename_pairs:
            names.append(file_reagent.stem)
            if file_reagent.stem != file_product.parts[-1][:-6]:
                breakpoint()

            atoms_reagent_ref = read(file_reagent)
            atoms_product_ref = read(file_product)

            E1_ref = atoms_product_ref.get_potential_energy()
            E0_ref = atoms_reagent_ref.get_potential_energy()
            E_reagent_ref.append(E0_ref)
            E_product_ref.append(E1_ref)

            dE_ref = E1_ref - E0_ref - reference_ref
            dEs_ref.append(dE_ref)

            subdir = file_reagent.relative_to(config.directory_in).parent
            subdirs.append(subdir)

            path_reagent = outdir / subdir / file_reagent.stem
            path_product = outdir / subdir / file_product.stem

            ct_reagent = read_from_CT_directory(path_reagent, model=model)
            ct_product = read_from_CT_directory(path_product, model=model)

            E1 = ct_product.atoms.get_potential_energy()
            E0 = ct_reagent.atoms.get_potential_energy()
            E_reagent.append(E0)
            E_product.append(E1)

            dE = E1 - E0 - reference
            dEs.append(dE)

            errors.append(dE - dE_ref)
            w = ct_reagent.omega
            w_global.append(w)

            if config.analysis.local_atom:
                i_local = atoms_reagent_ref.get_chemical_symbols().index(config.analysis.local_atom)
                i_distance = atoms_reagent_ref.get_chemical_symbols().index(config.analysis.distance_atom)

                w_i = ct_reagent.omega_local(i_local)
                dist = ct_reagent.atoms.get_distance(i_local, i_distance)
                w_dist = w_i / dist
                w_local.append(w_i)
                w_force.append(w_dist)

        data = dict(name=names, subdir=subdirs, E_reagent=E_reagent, E_product=E_product, E_reagent_ref=E_reagent_ref,
                    E_product_ref=E_product_ref, dE=dEs, dE_ref=dEs_ref, error=errors, w=w_global)
        if w_local:
            data['w_B'] = w_local
            if w_force:
                data['F_FLP'] = w_force
        df = pd.DataFrame(data=data)

        # energies -= dE_ref
        df.to_csv(f'FLP_{model}.csv')

        for subdir in df.subdir.unique():
            il = (df.subdir == subdir)
            # removing outliers
            il_corr = (df.error.abs() < 1) & (df.subdir == subdir)

            # correlate energies
            if i_model == 0:
                x = df.dE[il].values
                y = df.dE_ref[il].values
                plt.figure()
                plt.scatter(x, y, label=model)
                r_sq, intercept, slope = correlate(x, y)
                y_ = slope * np.array(x) + intercept
                plt.plot(x, y_,
                         label=f'$\mathrm{{\Delta E}}^{{DFT}}={slope:.2f}\mathrm{{\Delta E}}^{{AIMNet}}{intercept:+.2f}; r^2 = {r_sq:.2f}$')

                plt.xlabel(r'$\mathrm{\Delta E}^{AIMNet}$')
                plt.ylabel("$\mathrm{\Delta E}^{DFT}$")
                # plt.ylabel(f"$\mathrm{{{NE}}}^{{Mayr}}$")
                plt.legend(loc='best')
                plt.savefig(f'FLP_{subdir}.png')
                plt.close()

                # removing outliers
                x = df.dE[il_corr].values
                y = df.dE_ref[il_corr].values
                minval = min(x.min(), y.min())
                maxval = max(x.max(), y.max())
                d = (maxval - minval) * 0.1
                minmax = [minval - d, maxval + d]

                plt.figure()
                plt.scatter(x, y, label=model)
                r_sq, intercept, slope = correlate(x, y)
                y_ = slope * np.array(x) + intercept
                plt.plot(x, y_,
                         label=f'$\mathrm{{\Delta E}}^{{DFT}}={slope:.2f}\mathrm{{\Delta E}}^{{AIMNet}}{intercept:+.2f}; r^2 = {r_sq:.2f}$')

                plt.xlabel(r'$\mathrm{\Delta E}^{AIMNet}$')
                plt.ylabel("$\mathrm{\Delta E}^{DFT}$")
                # plt.ylabel(f"$\mathrm{{{NE}}}^{{Mayr}}$")
                plt.xlim(minmax)
                plt.ylim(minmax)
                plt.legend(loc='best')
                plt.savefig(f'FLP_{subdir}_corr.png')
                plt.close()

            # correlate descriptors
            y = df.dE_ref[il].values
            y_corr = df.dE_ref[il_corr].values
            for lbl in ['w', 'w_B', 'F_FLP']:
                x = df[lbl][il].values
                plt.figure()
                plt.scatter(x, y, label=model)
                r_sq, intercept, slope = correlate(x, y)
                y_ = slope * np.array(x) + intercept
                lblw = (lbl.replace('w', rf'\omega').replace('_FLP', '^{{FLP}}') + f'_{{{model}}}').replace('_B_{',
                                                                                                            '_{B,')
                label = f'$\Delta E^{{DFT}}={slope:.2f}{lblw}{intercept:+.2f}; r^2 = {r_sq:.2f}$'
                # breakpoint()
                plt.plot(x, y_, label=label)
                plt.xlabel(rf'$\mathrm{{{lblw}}}$')
                plt.ylabel("$\mathrm{\Delta E}^{DFT}$")
                # plt.ylabel(f"$\mathrm{{{NE}}}^{{Mayr}}$")
                plt.legend(loc='best')
                plt.savefig(f'FLP_{subdir}_{lbl}_{model}.png')
                plt.close()

                x = df[lbl][il_corr].values
                plt.figure()
                plt.scatter(x, y_corr, label=model)
                r_sq, intercept, slope = correlate(x, y_corr)
                y_ = slope * np.array(x) + intercept
                label = f'$\Delta E^{{DFT}}={slope:.2f}{lblw}{intercept:+.2f}; r^2 = {r_sq:.2f}$'
                plt.plot(x, y_, label=label)
                plt.xlabel(rf'$\mathrm{{{lblw}}}$')
                plt.ylabel("$\mathrm{\Delta E}^{DFT}$")
                # plt.ylabel(f"$\mathrm{{{NE}}}^{{Mayr}}$")
                plt.legend(loc='best')
                plt.savefig(f'FLP_{subdir}_{lbl}_{model}_corr.png')
                plt.close()

    print('... done.')


def report(config: RunConfiguration):
    directory = config.directory_out
    model = config.analysis.model
    text = ''
    c_A = 0
    c_I = 0
    n_chargedA = 0
    n_chargedI = 0
    n_molecules = 0
    for filename in config.filenames:
        name = filename.stem
        if name in config.analysis.ignore:
            continue
        outdir = directory / name
        try:
            ct = read_from_CT_directory(outdir, model=model)
        except FileNotFoundError:
            continue
        except KeyError:
            print(f'no charge {name}')
            continue
        charge = ct.atoms.charge
        n_molecules += 1
        addtext = ''
        if ct.A < 0:
            addtext += f'\tA = {ct.A:.2f}\n'
            c_A += 1
            n_chargedA += charge != 0
        if ct.I < 0:
            addtext += f'\tI = {ct.I:.2f}\n'
            c_I += 1
            n_chargedI += charge != 0
        if addtext:
            text += f'{name} charge = {charge}\n{addtext}\n'

    text += (f'{n_molecules} molecules\n'
             f'{c_I} where I < 0, {n_chargedI} charged\n'
             f'{c_A} where A < 0, {n_chargedA} charged')

    with open('report.txt', 'w') as f:
        print(text, file=f)

    # TODO: create csv file


def outputs2extxyz(config: RunConfiguration):
    """
    Read outputs to write extxyz files for ChargeTransfer calculations
    :param config:
    :return:
    """
    directory = config.directory_out
    filenames = list(directory.rglob(f'*{config.calc_out_suffix}'))
    for filename in tqdm(filenames, desc=f'Converting {config.calc_out_suffix} to .xyz'):
        try:
            atoms = read_gaussian_out(filename)
        except StopIteration:
            continue
        if 'charges' in atoms.arrays:
            del atoms.arrays['charges']
        atoms.write(filename.parent / f'{filename.stem}.xyz')


def correlate(x, y):
    x = np.reshape(x, (-1, 1))
    model = LinearRegression()
    model.fit(x, y)
    r_sq = model.score(x, y)
    intercept = model.intercept_
    slope = model.coef_[0]
    return r_sq, intercept, slope
